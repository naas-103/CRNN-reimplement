keras==3.1.1
matplotlib==3.8.4
mne==1.6.1
pyEDFlib==0.1.37
scikit_learn==1.4.1.post1
tensorflow==2.16.1
tensorflow_intel==2.16.1
torch==2.2.2
