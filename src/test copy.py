import numpy as np

pred_path = r"C:\Users\Nguyen\Desktop\CRNN-reimplement\src\src\out_sleepedfx\train\prop\predict\SC4572F0-pred.npz"
pred = np.load(pred_path)
print(pred["y_true"])
print("--------")
print(pred["y_pred"])
temp = 0

for i in range(len(pred["y_true"])):
    if pred["y_true"][i] != pred["y_pred"][i]:
        print(f"mismatch predict at {i}")
        temp += 1

print(f"total mismatch predict: {temp} on a total of {len(pred["y_pred"])} predicts")