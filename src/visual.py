import pandas as pd
import matplotlib.pyplot as plt
import os


train_acc_path = r"C:\Users\Nguyen\Desktop\CRNN-reimplement\src\src\out_sleepedfx\train\baseline\run-baseline_train-tag-epoch_accuracy.csv"
val_acc_path = r"C:\Users\Nguyen\Desktop\CRNN-reimplement\src\src\out_sleepedfx\train\baseline\run-baseline_validation-tag-epoch_accuracy.csv"
train_loss_path = r"C:\Users\Nguyen\Desktop\CRNN-reimplement\src\src\out_sleepedfx\train\baseline\run-baseline_train-tag-epoch_loss (1).csv"
val_loss_path = r"C:\Users\Nguyen\Desktop\CRNN-reimplement\src\src\out_sleepedfx\train\baseline\run-baseline_validation-tag-epoch_loss.csv"

train_acc = pd.read_csv(train_acc_path)
train_loss = pd.read_csv(train_loss_path)
val_acc = pd.read_csv(val_acc_path)
val_loss = pd.read_csv(val_loss_path)

# print(val_acc["Value"])

plt.plot(train_loss["Value"], label="train_loss")
plt.plot(val_loss["Value"], label='val_loss')
plt.legend()
plt.title("Giá trị hàm mất mát")
plt.grid(True)
plt.show()
