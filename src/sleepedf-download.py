import os 
import wget
import hashlib
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

url = "https://www.physionet.org/files/sleep-edfx/1.0.0"
o_dir = r'C:\Users\Nguyen\Desktop\CRNN-reimplement\data\sleepedfx'
sum_file = r'C:\Users\Nguyen\Desktop\CRNN-reimplement\data\sleepedfx\SHA256SUMS.txt'
if not os.path.isdir(o_dir):
    os.makedirs(o_dir)


with open(sum_file) as f:
    for l in f.readlines():
        l = l.strip()
        tmp = l.split(" ")

        hash = tmp[0]
        fname = tmp[len(tmp)-1]

        if 'sleep-cassette' in fname:
            download_url = url+"/"+fname
            save_f = os.path.join(o_dir, fname)
            save_dir = os.path.dirname(save_f)

            if not os.path.exists(save_dir):
                os.makedirs(save_dir)
            
            if not os.path.isfile(save_f):
                print(f"\nDownloading {download_url} to {save_f}")
                wget.download(download_url, save_f)

            with open(save_f, "rb") as ff:
                b = ff.read()
                readable_hash = hashlib.sha256(b).hexdigest()
                assert hash == readable_hash

                print(f'Downloaded {save_f}')