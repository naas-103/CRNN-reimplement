import numpy as np
import pywt
import timeit
import pyedflib
import glob
import os
import logging
import ntpath
import pandas as pd

logger = logging.getLogger(__name__)
log_file = "../../log/feature-extraction.log"
os.makedirs(os.path.dirname(log_file), exist_ok=True)
logging.basicConfig(filename=log_file, 
                    filemode="a",
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.INFO
)

ann2label = {
    "Sleep stage W": 0,
    "Sleep stage 1": 1,
    "Sleep stage 2": 2,
    "Sleep stage 3": 3, "Sleep stage 4": 3, # Follow AASM Manual
    "Sleep stage R": 4,
    "Sleep stage ?": 6,
    "Movement time": 5
}

# Mean
def mean(x):
    mean_value = []
    for a in x:
        mean_value.append(np.mean(a))
    return mean_value

#Energy
def energy(x):
    energy = []
    for a in x:
        energy.append(np.sum(np.square(a)))
    return energy


#Standard Deviation
def std(x):
    std = []
    for a in x:
        std.append(np.std(a))
    return std

#Variance
def variance(x):
    var = []
    for a in x:
        var.append(np.var(a))
    return var

#Mean Abolute Value
def absolute_value(x):
    av = []
    for a in x:
        av.append(np.mean(np.absolute(a)))
    return av

#Average Power
def average_power(x):
    av_power = []
    for a in x:
        av_power.append(np.mean(np.square(a)))
    return av_power

#DWT decomposition
def decomposition(signal):
    coeffs = pywt.wavedec(signal, 'db4', level=4)
    return coeffs


def main():
    output_dir = r"../../data/sleepedfx/feature"
    data_dir = r"../../data/sleepedfx/sleep-cassette"
    channel = "EEG Fpz-Cz"
    start = timeit.default_timer()

    psg_name = glob.glob(os.path.join(data_dir, "*PSG.edf"))
    anno_name = glob.glob(os.path.join(data_dir,"*Hypnogram.edf"))

    logging.info("------files for pre-processing------")
    logging.info(f'{psg_name}')

    psg_name.sort()
    anno_name.sort()

    psg_name = np.asarray(psg_name)
    anno_name = np.asarray(anno_name)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for i in range(len(psg_name)):
        psg_sheet = pyedflib.EdfReader(psg_name[i])
        anno_sheet = pyedflib.EdfReader(anno_name[i])

        logger.info(f"working on {psg_name[i]} and {anno_name[i]}")

        assert psg_sheet.getStartdatetime() == anno_sheet.getStartdatetime(), f"please check the integrity of dataset as psg and annotation start time do not match:{psg_name[i]}, {anno_name[i]}"
    
        duration = psg_sheet.getFileDuration()
        epoch_duration = psg_sheet.datarecord_duration
        logging.info(f"Epoch duration: {epoch_duration}")

        psg_channel = psg_sheet.getSignalLabels()
        psg_ch_samples = psg_sheet.getNSamples()

        channel_idx = -1

        for s in range(psg_sheet.signals_in_file):
            if psg_channel[s] == channel:
                channel_idx = s
                break
        
        if channel_idx == -1:
            raise Exception("Channel not found.")
        
        sampling_rate = psg_sheet.getSampleFrequency(channel_idx)
        n_epoch_samples = int(epoch_duration*sampling_rate)
        signals = psg_sheet.readSignal(channel_idx).reshape(-1, n_epoch_samples)

        logging.info(f"Selected channel: {channel}.")
        logging.info(f"Selected channel's samples: {psg_ch_samples[channel_idx]}.")
        logging.info(f"Sampling rate: {sampling_rate}.")

        labels = []
        total_duration = 0
        anno_onset, anno_duration, anno_stage = anno_sheet.readAnnotations()

        for j in range(len(anno_stage)):
            onset_sec = int(anno_onset[j])
            duration_sec = int(anno_duration[j])
            anno_str = anno_stage[j]

            assert onset_sec == total_duration

            label = ann2label[anno_str]

            if duration_sec % epoch_duration != 0:
                raise Exception(f"Something wrong: {duration_sec} {epoch_duration}")

            epoch_number = int(duration_sec / epoch_duration)

            label_epoch = np.ones(epoch_number, dtype=int) * label
            labels.append(label_epoch)

            total_duration += duration_sec
            logging.info(f"Onset: {onset_sec}, duration: {duration_sec}, labels: {label} ({anno_str})")

        labels = np.hstack(labels)

        labels = labels[:len(signals)]
        
        x = signals.astype(np.float32)
        y = pd.DataFrame(labels, columns=['y'])

        cA = []
        cD1 = []
        cD2 = []
        cD3 = []
        cD4 = []
        for idx, signal in enumerate(x):
            coeffs = decomposition(signal)
            cA.append(coeffs[0])
            cD1.append(coeffs[1])
            cD2.append(coeffs[2])
            cD3.append(coeffs[3])
            cD4.append(coeffs[4])
        
        #cA feature
        mean_cA = mean(cA)
        energy_cA = energy(cA)
        std_cA = std(cA)
        var_cA = variance(cA)
        absvalue_cA = absolute_value(cA)
        avgpower_cA = average_power(cA)

        #Feature extraction from cD1
        mean_cD1 = mean(cD1)
        energy_cD1 = energy(cD1)
        std_cD1 = std(cD1)
        var_cD1 = variance(cD1)
        absvalue_cD1 = absolute_value(cD1)
        avgpower_cD1 = average_power(cD1)

        #Feature extraction from cD2
        mean_cD2 = mean(cD2)
        energy_cD2 = energy(cD2)
        std_cD2 = std(cD2)
        var_cD2 = variance(cD2)
        absvalue_cD2 = absolute_value(cD2)
        avgpower_cD2 = average_power(cD2)

        #Feature extraction from cD3
        mean_cD3 = mean(cD3)
        energy_cD3 = energy(cD3)
        std_cD3 = std(cD3)
        var_cD3 = variance(cD3)
        absvalue_cD3 = absolute_value(cD3)
        avgpower_cD3 = average_power(cD3)

        #Feature extraction from cD4
        mean_cD4 = mean(cD4)
        energy_cD4 = energy(cD4)
        std_cD4 = std(cD4)
        var_cD4 = variance(cD4)
        absvalue_cD4 = absolute_value(cD4)
        avgpower_cD4 = average_power(cD4)

        features = pd.DataFrame({'mean_cA': mean_cA,'energy_cA': energy_cA,'std_cA':std_cA,
                                'var_cA':var_cA,'absvalue_cA':absvalue_cA,'avgpower_cA':avgpower_cA,

                                'mean_cD1': mean_cD1,'energy_cD1': energy_cD1,'std_cD1':std_cD1,
                                'var_cD1':var_cD1,'absvalue_cD1':absvalue_cD1,'avgpower_cD1':avgpower_cD1,

                                'mean_cD2': mean_cD2,'energy_cD2': energy_cD2,'std_cD2':std_cD2,
                                'var_cD2':var_cD2,'absvalue_cD2':absvalue_cD2,'avgpower_cD2':avgpower_cD2,

                                'mean_cD3': mean_cD3,'energy_cD3': energy_cD3,'std_cD3':std_cD3,
                                'var_cD3':var_cD3,'absvalue_cD3':absvalue_cD3,'avgpower_cD3':avgpower_cD3,

                                'mean_cD4': mean_cD4,'energy_cD4': energy_cD4,'std_cD4':std_cD4,
                                'var_cD4':var_cD4,'absvalue_cD4':absvalue_cD4,'avgpower_cD4':avgpower_cD4
                                })

        #Add the labels to the features dataframe
        features = pd.concat([features, y], axis=1, sort=False)

        #creating csv file of features
        features.to_csv(os.path.join(output_dir, rf"{ntpath.basename(psg_name[i])}_feature.csv"), sep=',',index=False, encoding='utf-8')

if __name__ == '__main__':
    main()