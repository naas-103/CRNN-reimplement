from baseline import EEGNet, aggregateData
from sklearn.metrics import classification_report, confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sns
from sleepedfx import train
import numpy as np
import pandas as pd
import tensorflow as tf
from scipy.signal import savgol_filter
from scipy.fft import fft, fftshift
_, _, _, _, test_x, test_y = aggregateData()

model = EEGNet(nb_classes = 5, Chans = train["n_channels"], Samples = train["input_size"],
            dropoutRate = 0.5, kernLength = 32, F1 = 8, D = 2, F2 = 16,
            dropoutType = 'Dropout')

print(model)
model.load_weights(r'src\out_sleepedfx\train\baseline\checkpoint.keras')
test_x = np.expand_dims(test_x, axis=1)
test_x = np.expand_dims(test_x, axis=-1)
# # dataset = tf.data.Dataset.from_tensor_slices(test_x)

# # print(dataset)
# print(np.shape(test_x))
y_pred = model.predict(test_x)
y_pred = np.argmax(y_pred, axis=-1)

print(classification_report(y_true=test_y,y_pred=y_pred,target_names=['WAKE', 'NREM1', 'NREM2', 'NREM3', 'REM']))
cm = confusion_matrix(test_y, y_pred)
cm = pd.DataFrame(cm, range(5),range(5))
plt.figure(figsize = (8,6))

sns.heatmap(cm, annot=True, fmt="d", cmap="Blues", xticklabels=["WAKE", "NREM1", "NREM2", "NREM3", "REM"], yticklabels=["WAKE", "NREM1", "NREM2", "NREM3", "REM"]), # font size
plt.xlabel("Dự đoán")
plt.ylabel("Thực tế")
plt.show()
print(model.layers)
# filters = model.layers[2].get_weights()[0]
# print(filters)
# filter_size, filter_depth, num_filters = filters.shape
# print(filter_size)

# filtered_fourier_filters = []



# for i in range(num_filters):
#     filter_fft = fft(filters[:,:,i])  # Compute the 1D FFT
#     shifted_fft = fftshift(filter_fft)  # Shift the zero-frequency component to the center
#     shifted_fft = np.transpose(shifted_fft)
#     # Apply Savitzky-Golay filter to the Fourier transformed filter
#     # print(shifted_fft.shape)
#     smoothed_fft = savgol_filter(np.real(shifted_fft), window_length=5, polyorder=2)
#     smoothed_fft = smoothed_fft + 1j * savgol_filter(np.imag(shifted_fft), window_length=5, polyorder=2)
#     # print(smoothed_fft.shape)
#     filtered_fourier_filters.append(smoothed_fft)
    

# # Convert the list to a NumPy array
# filtered_fourier_filters = np.array(filtered_fourier_filters)
# # np.squeeze(filtered_fourier_filters)
# print(filtered_fourier_filters.shape)

# # freqs = np.fft.fftfreq(filter_size, d=1)
# # plt.plot(np.transpose(np.abs(filtered_fourier_filters[0])), freqs)
# fig, axs = plt.subplots(28, 2, figsize=(15, 12))
# for i in range(100,128):
#         freqs = np.fft.fftfreq(filter_size, d=1/100)
#         freqs = freqs[freqs >= 0]
#         pos_indices = np.arange(len(freqs))
#         axs[i-100, 0].plot(freqs, np.transpose(np.abs(np.take(filtered_fourier_filters[i], pos_indices))))
#         axs[i-100, 0].set_title(f'Filter {i+1} (Magnitude Spectrum)')
#         axs[i-100, 1].plot(freqs, np.transpose(np.abs(np.take(filtered_fourier_filters[i], pos_indices))))
#         axs[i-100, 1].set_title(f'Filter {i+1} (Phase Spectrum)')
# # plt.tight_layout()
# plt.show()