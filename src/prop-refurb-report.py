from improv import PropModel, aggregateData
from sklearn.metrics import classification_report, confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sns
from sleepedfx import train
import numpy as np
import pandas as pd
import tensorflow as tf
from scipy.signal import savgol_filter
from scipy.fft import fft, fftshift
from keras import layers
import keras
_, _, _, _, test_x, test_y = aggregateData()

# model = EEGNet(nb_classes = 5, Chans = train["n_channels"], Samples = train["input_size"],
#             dropoutRate = 0.5, kernLength = 32, F1 = 8, D = 2, F2 = 16,
#             dropoutType = 'Dropout')
fs = train["sampling_rate"]
model = keras.Sequential(
[   
    # layers.Conv1D(filters=128, kernel_size=int(fs/2), strides=int(fs/16), use_bias=False, padding='same'),
    # layers.BatchNormalization(),
    layers.Conv1D(filters=128, kernel_size=int(fs/2), strides=int(fs/16), use_bias=False, padding='same'),
    layers.BatchNormalization(),
    layers.Activation(tf.nn.leaky_relu),

    layers.MaxPooling1D(pool_size=8, strides=8),
    layers.Dropout(rate=0.5),

    layers.Conv1D(filters=128, kernel_size=8, strides=1, use_bias=False, padding='same'),
    layers.BatchNormalization(),
    layers.Activation(tf.nn.leaky_relu),

    layers.Conv1D(filters=128, kernel_size=8, strides=1, use_bias=False, padding='same'),
    layers.BatchNormalization(),
    layers.Activation(tf.nn.leaky_relu),

    layers.Conv1D(filters=128, kernel_size=8, strides=1, use_bias=False, padding='same'),
    layers.BatchNormalization(),
    layers.Activation(tf.nn.leaky_relu),

    layers.MaxPooling1D(pool_size=4, strides=4, padding='same'),
    # layers.Flatten(),
    layers.Dropout(rate=0.5), 

    # layers.RNN(cell=layers.LSTMCell(self.config['n_rnn_units'], unit_forget_bias=1.0), return_sequences=False, return_state=True, stateful=True),
    layers.LSTM(units=train['n_rnn_units']),
    layers.Dropout(0.5),
    layers.Dense(5, use_bias=False, kernel_initializer=keras.initializers.VarianceScaling(scale=0.1, mode='fan_in', distribution='normal'), activation="softmax"),
], name="Classificator"
)
print(model)
model(layers.Input((3000,1), 1))
model.load_weights(r"C:\Users\Nguyen\Desktop\CRNN-reimplement\src\src\out_sleepedfx\train\prop\prop.keras")
# test_x = np.expand_dims(test_x, axis=1)
test_x = np.expand_dims(test_x, axis=-1)
# # dataset = tf.data.Dataset.from_tensor_slices(test_x)

# # print(dataset)
# print(np.shape(test_x))
y_pred = model.predict(test_x)
y_pred = np.argmax(y_pred, axis=-1)

print(classification_report(y_true=test_y,y_pred=y_pred,target_names=['WAKE', 'NREM1', 'NREM2', 'NREM3', 'REM']))
cm = confusion_matrix(test_y, y_pred)
cm = pd.DataFrame(cm, range(5),range(5))
plt.figure(figsize = (8,6))

sns.heatmap(cm, annot=True, fmt="d", cmap="Blues", xticklabels=["WAKE", "NREM1", "NREM2", "NREM3", "REM"], yticklabels=["WAKE", "NREM1", "NREM2", "NREM3", "REM"]), # font size
plt.xlabel("Dự đoán")
plt.ylabel("Thực tế")
plt.show()
print(model.layers)