import glob
import os
import numpy as np
import pyedflib
import logging
import ntpath

from sleepstage import stage_dict
logger = logging.getLogger(__name__)
log_file = "../log/sleepedfx-processing.log"
os.makedirs(os.path.dirname(log_file), exist_ok=True)
logging.basicConfig(filename=log_file, 
                    filemode="a",
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.INFO
)

ann2label = {
    "Sleep stage W": 0,
    "Sleep stage 1": 1,
    "Sleep stage 2": 2,
    "Sleep stage 3": 3, "Sleep stage 4": 3, # Follow AASM Manual
    "Sleep stage R": 4,
    "Sleep stage ?": 6,
    "Movement time": 5
}

def main():
    #data_dir = os.path.join(os.path.expanduser("~"),"Documents","KLTN","data","sleepedf","sleep-cassette")
    # output_dir = os.path.join(os.path.expanduser("~"),"Documents","KLTN","data","sleepedf","processed")
    # data_dir = os.path.join(os.path.expanduser("~"),"Documents","KLTN","data","sample")
    data_dir = r"../data/sleepedfx/sleep-cassette"
    output_dir = r"../data/sleepedfx-output"
    channel = "EEG Fpz-Cz"

    psg_name = glob.glob(os.path.join(data_dir, "*PSG.edf"))
    anno_name = glob.glob(os.path.join(data_dir,"*Hypnogram.edf"))
    logging.info("------files for pre-processing------")
    logging.info(f'{psg_name}')

    psg_name.sort()
    anno_name.sort()

    psg_name = np.asarray(psg_name)
    anno_name = np.asarray(anno_name)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for i in range(len(psg_name)):
        psg_sheet = pyedflib.EdfReader(psg_name[i])
        anno_sheet = pyedflib.EdfReader(anno_name[i])

        #logging.info(psg_sheet.getHeader())
        logging.info(f"working on {psg_name[i]} and {anno_name[i]}")
        
        assert psg_sheet.getStartdatetime() == anno_sheet.getStartdatetime(), f"please check the integrity of dataset as psg and annotation start time do not match:{psg_name[i]}, {anno_name[i]}"
        

        duration = psg_sheet.getFileDuration()
        epoch_duration = psg_sheet.datarecord_duration
        logging.info(f"Epoch duration: {epoch_duration}")

        psg_channel = psg_sheet.getSignalLabels()
        psg_ch_samples = psg_sheet.getNSamples()

        channel_idx = -1

        for s in range(psg_sheet.signals_in_file):
            if psg_channel[s] == channel:
                channel_idx = s
                break
        
        if channel_idx == -1:
            raise Exception("Channel not found.")
        
        sampling_rate = psg_sheet.getSampleFrequency(channel_idx)
        n_epoch_samples = int(epoch_duration*sampling_rate)
        signals = psg_sheet.readSignal(channel_idx).reshape(-1, n_epoch_samples)

        logging.info(f"Selected channel: {channel}.")
        logging.info(f"Selected channel's samples: {psg_ch_samples[channel_idx]}.")
        logging.info(f"Sampling rate: {sampling_rate}.")

        n_epochs  = psg_sheet.datarecords_in_file
        # if epoch_duration == 60:
        #     n_epochs = n_epochs * 2
        # assert len(signals) == n_epochs, f"signal: {signals.shape} != {n_epochs}"

        labels = []
        total_duration = 0
        anno_onset, anno_duration, anno_stage = anno_sheet.readAnnotations()

        for j in range(len(anno_stage)):
            onset_sec = int(anno_onset[j])
            duration_sec = int(anno_duration[j])
            anno_str = anno_stage[j]

            assert onset_sec == total_duration

            label = ann2label[anno_str]

            if duration_sec % epoch_duration != 0:
                raise Exception(f"Something wrong: {duration_sec} {epoch_duration}")

            epoch_number = int(duration_sec / epoch_duration)

            label_epoch = np.ones(epoch_number, dtype=int) * label
            labels.append(label_epoch)

            total_duration += duration_sec
            logging.info(f"Onset: {onset_sec}, duration: {duration_sec}, labels: {label} ({anno_str})")

        labels = np.hstack(labels)

        labels = labels[:len(signals)]
        
        x = signals.astype(np.float32)
        y = labels.astype(np.int32)


        # Select only sleep periods
        w_edge_mins = 30
        nw_idx = np.where(y != stage_dict["W"])[0]
        start_idx = nw_idx[0] - (w_edge_mins * 2)
        end_idx = nw_idx[-1] + (w_edge_mins * 2)
        if start_idx < 0: start_idx = 0
        if end_idx >= len(y): end_idx = len(y) - 1
        select_idx = np.arange(start_idx, end_idx+1)
        logging.info("Data before selection: {}, {}".format(x.shape, y.shape))
        x = x[select_idx]
        y = y[select_idx]
        logging.info("Data after selection: {}, {}".format(x.shape, y.shape))

        # Remove movement and unknown
        move_idx = np.where(y == stage_dict["MOVE"])[0]
        unk_idx = np.where(y == stage_dict["UNK"])[0]
        if len(move_idx) > 0 or len(unk_idx) > 0:
            remove_idx = np.union1d(move_idx, unk_idx)
            logging.info("Remove irrelavant stages")
            logging.info("  Movement: ({}) {}".format(len(move_idx), move_idx))
            logging.info("  Unknown: ({}) {}".format(len(unk_idx), unk_idx))
            logging.info("  Remove: ({}) {}".format(len(remove_idx), remove_idx))
            logging.info("  Data before removal: {}, {}".format(x.shape, y.shape))
            select_idx = np.setdiff1d(np.arange(len(x)), remove_idx)
            x = x[select_idx]
            y = y[select_idx]
            logging.info("  Data after removal: {}, {}".format(x.shape, y.shape))

        filename = ntpath.basename(psg_name[i]).replace("-PSG.edf", ".npz")
        save_dict = {
            "x": x, 
            "y": y, 
            "fs": sampling_rate,
            "ch_label": channel,
            "start_datetime": psg_sheet.getStartdatetime(),
            "file_duration": duration,
            "epoch_duration": epoch_duration,
            "n_all_epochs": n_epochs,
            "n_epochs": len(x),
        }

        if np.savez(os.path.join(output_dir, filename), **save_dict):
            logging.info(f"file saved at {os.path.join(output_dir, filename)}")

if __name__ == '__main__':
    main()