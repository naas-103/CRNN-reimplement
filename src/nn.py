import tensorflow as tf
import keras
from keras import layers
import numpy as np
import timeit
from sleepedfx import train
import os
import sklearn.metrics as skmetrics
import logging
import datetime


nn_log = "../log/nn.log"
os.makedirs(os.path.dirname(nn_log), exist_ok=True)
logger = logging.getLogger(__name__)
logging.basicConfig(filename=nn_log, 
                    filemode="a",
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.INFO
)

# tf.debugging.experimental.enable_dump_debug_info(logdir, tensor_debug_mode="FULL_HEALTH", circular_buffer_size=-1)

# data_path = r"C:\Users\Nguyen\Desktop\CRNN-reimplement\data\sample-output\SC4001E0.npz"
# eval_path = r"C:\Users\Nguyen\Desktop\CRNN-reimplement\data\sample-output\SC4002E0.npz"

# raw_data = np.load(data_path)
# raw_eval = np.load(eval_path)
# train_x = raw_data['x']
# train_x = np.squeeze(train_x)
# train_x = train_x[:, :, tf.newaxis]
# train_x = train_x.astype(np.float32)
# train_y = raw_data['y']
# train_y = np.squeeze(train_y)
# # eval_x = np.squeeze(raw_eval['x'])
# # eval_y = np.squeeze(raw_eval['y'])
# # fs = raw['fs']
# print(train_x.shape)


# class Model(keras.Model):
#         def __init__(self, config=None):
#             super().__init__()
#             self.config = config
#             fs = self.config["sampling_rate"]
#             #defining convolutional layer 
#             layers.Conv1D(filters=128, kernel_size=int(fs/2), strides=int(fs/16), use_bias=False, padding='same'),
#             layers.BatchNormalization(),
#             layers.Conv1D(filters=128, kernel_size=int(fs/2), strides=int(fs/16), use_bias=False, padding='same'),
#             layers.BatchNormalization(),
#             layers.Activation(tf.nn.leaky_relu),

#             layers.MaxPooling1D(pool_size=8, strides=8),
#             layers.Dropout(rate=0.5),

#             layers.Conv1D(filters=128, kernel_size=8, strides=1, use_bias=False, padding='same'),
#             layers.BatchNormalization(),
#             layers.Activation(tf.nn.leaky_relu),

#             layers.Conv1D(filters=128, kernel_size=8, strides=1, use_bias=False, padding='same'),
#             layers.BatchNormalization(),
#             layers.Activation(tf.nn.leaky_relu),

#             layers.Conv1D(filters=128, kernel_size=8, strides=1, use_bias=False, padding='same'),
#             layers.BatchNormalization(),
#             layers.Activation(tf.nn.leaky_relu),

#             layers.MaxPooling1D(pool_size=4, strides=4, padding='same'),
#             layers.Dropout(rate=0.5),

#             self.rnn = layers.RNN(cell=layers.LSTMCell(self.config['n_rnn_units'], unit_forget_bias=1.0), return_sequences=True, return_state=True, stateful=True)

#             self.classification = keras.Sequential([
#                 layers.Flatten(),
#                 layers.Dense(5, use_bias=False, kernel_initializer=keras.initializers.VarianceScaling(scale=0.1, mode='fan_in', distribution='normal')),
#                 layers.Softmax()
#             ], name = "MLP")

        

#         #rewrite the call method (required)
#         def call(self, inputs):
#             # x = inputs[:, :, tf.newaxis]
#             x = self.cnn(inputs)
#             # # x = tf.reshape(x, shape=[-1, self.config['seq_length'], 2048])
#             # # x, self.final_state = self.create_rnn(x)
#             # # x.reshape(-1, self.config['r_rnn_units'])
#             # x = self.rnn_dropout(x)
#             seq, self.state = self.rnn(x)
#             return self.classification(seq)
        
class Network(tf.Module):
    def __init__(self, use_best=False, config=None, output_dir="../output", training=True, testing=False):
        super().__init__()
        self.config = config
        self.output_dir = output_dir
        self.checkpoint_path = os.path.join(self.output_dir, "checkpoint")
        self.best_ckpt_path = os.path.join(self.output_dir, "best_ckpt")
        self.weights_path = os.path.join(self.output_dir, "weights")
        self.log_dir = os.path.join(self.output_dir, "log")
        self.use_best = use_best
        fs = self.config["sampling_rate"]

        #define model variables
        self.global_step = tf.Variable(0, trainable=False, name="global_step", dtype=np.int64)
        self.global_epoch = 0

        # define model optimizer
        # self.optimizer= 
        self.optimizer = keras.optimizers.Adam(learning_rate=self.config["learning_rate"], beta_1=self.config["adam_beta_1"], beta_2=self.config["adam_beta_2"], epsilon=self.config["adam_epsilon"])
        

        # define model loss function 
        self.loss_fn = keras.losses.sparse_categorical_crossentropy


        #keep reading tensorboard, not done yet
        self.train_writer = tf.summary.create_file_writer(self.output_dir)
        # self.test_train_writer = tf.summary.create_file_writer(logdir="../test-log/train",experimental_trackable=True)

        #instantatiate a model
        self.Model = keras.Sequential(
            [   
                # layers.Conv1D(filters=128, kernel_size=int(fs/2), strides=int(fs/16), use_bias=False, padding='same'),
                # layers.BatchNormalization(),
                layers.Conv1D(filters=128, kernel_size=int(fs/2), strides=int(fs/16), use_bias=False, padding='same'),
                layers.BatchNormalization(),
                layers.Activation(tf.nn.leaky_relu),

                layers.MaxPooling1D(pool_size=8, strides=8),
                layers.Dropout(rate=0.5),

                layers.Conv1D(filters=128, kernel_size=8, strides=1, use_bias=False, padding='same'),
                layers.BatchNormalization(),
                layers.Activation(tf.nn.leaky_relu),

                layers.Conv1D(filters=128, kernel_size=8, strides=1, use_bias=False, padding='same'),
                layers.BatchNormalization(),
                layers.Activation(tf.nn.leaky_relu),

                layers.Conv1D(filters=128, kernel_size=8, strides=1, use_bias=False, padding='same'),
                layers.BatchNormalization(),
                layers.Activation(tf.nn.leaky_relu),

                layers.MaxPooling1D(pool_size=4, strides=4, padding='same'),
                # layers.Flatten(),
                layers.Dropout(rate=0.5), 

                # layers.RNN(cell=layers.LSTMCell(self.config['n_rnn_units'], unit_forget_bias=1.0), return_sequences=False, return_state=True, stateful=True),
                layers.LSTM(units=config['n_rnn_units']),
                layers.Dropout(0.5),
                layers.Dense(5, use_bias=False, kernel_initializer=keras.initializers.VarianceScaling(scale=0.1, mode='fan_in', distribution='normal'), activation="softmax"),
            ], trainable=training, name="Classificator"
        )

        #instantiate a checkpoint for model
        self.checkpoint = tf.train.Checkpoint(step=self.global_step, model=self.Model)
        self.ckpt_mngr = tf.train.CheckpointManager(self.checkpoint, self.best_ckpt_path, max_to_keep=3)

        if self.use_best and self.ckpt_mngr.latest_checkpoint:
            self.checkpoint.restore(self.ckpt_mngr.latest_checkpoint).expect_partial().assert_existing_objects_matched()
            # if testing:
            #     tf.reshape(self.Model.layers[-3].states[0], (1, -1))
            logger.info("Restored from {}".format(self.ckpt_mngr.latest_checkpoint))
        else:
            logger.info("Initializing from scratch")

    def train_with_minibatches(self, minibatch):  
        start = timeit.default_timer()
        preds, trues, losses, outputs = ([], [], [], {})
        logger.info("-----begin training-----")
        for x, y, w, sl, re in minibatch:
            with tf.GradientTape() as g:
                # logger.info(f"train feature size: {x.shape}")
                x = tf.convert_to_tensor(x)
                x = x[:, :, tf.newaxis]
                y = tf.convert_to_tensor(y)
                w = tf.convert_to_tensor(w)
                y_pred_temp = self.Model(x)
                # print(f"y_pred_temp shape={y_pred_temp.shape}")
                # y_pred = tf.argmax(y_pred_temp, axis=1)
                # print(f"y_pred shape={y_pred.shape}")
                # print(f"y sample {y[:]}")
                loss = self.loss_fn(y_true=y, y_pred=y_pred_temp)
                # loss = loss[:, tf.newaxis]
                # print(f"loss shape {loss.shape}")
                # print(f"weight shape {w.shape}")
                # w = w[tf.newaxis,:]
                loss = tf.multiply(loss, w)

                sample_weight = tf.reduce_sum(
                    tf.multiply(
                        tf.one_hot(indices=y, depth=self.config["n_classes"]),
                        np.asarray(self.config["class_weights"], dtype=np.float32)
                    ), 1
                )
                
                loss_w_class = tf.multiply(loss, sample_weight)
                loss = tf.reduce_sum(loss_w_class) / tf.reduce_sum(w)

                cnn_weight = [param for layer in self.Model.layers if isinstance(layer, keras.layers.Conv1D) for param in layer.trainable_weights]
                reg_loss = 0
                for p in cnn_weight:
                    reg_loss += tf.reduce_sum(p ** 2) / 2

                reg_loss = self.config["l2_weight_decay"] * reg_loss
                ce_loss = loss
                loss = loss + reg_loss
            # print(f"trainable: {self.model.trainable_variables}")
            gradients = g.gradient(loss, self.Model.trainable_weights)
            gradients = [tf.clip_by_norm(grad, clip_norm=self.config["clip_grad_value"]) for grad in gradients]
            # print(f"gradient values: {gradients}")
            self.optimizer.apply(gradients, self.Model.trainable_weights)
            # print(self.optimizer)
            # self.optimizer.apply_gradients(zip(gradients, self.Model.trainable_variables))

            losses.append(loss)

            self.checkpoint.step.assign_add(1)
            # if (self.checkpoint.step % 15 == 0):
            #     save_path = self.ckpt_mngr.save()
            #     logger.info("Saved checkpoint for step {}: {}".format(int(self.checkpoint.step), save_path))
            tmp_pred = np.reshape(tf.argmax(y_pred_temp, axis=1), (self.config['batch_size'], self.config['seq_length']))
            temp_trues = np.reshape(y, (self.config["batch_size"], self.config["seq_length"]))

            for i in range(self.config["batch_size"]):
                preds.extend(tmp_pred[i, :sl[i]])
                trues.extend(temp_trues[i, :sl[i]])
    
        acc = skmetrics.accuracy_score(y_true=trues, y_pred=preds)
        f1_score = skmetrics.f1_score(y_true=trues, y_pred=preds, average="macro")
        all_loss = np.array(losses).mean()
        cm = skmetrics.confusion_matrix(y_true=trues, y_pred=preds, labels=[0,1,2,3,4])
        stop = timeit.default_timer()
        duration = stop - start
        # with self.test_train_writer.as_default():
        #     tf.summary.scalar("mean loss", data=all_loss, step=1)
        #     print("-----data logged-----")
        logger.info(f"time elapsed: {duration}s")
        outputs.update({
            "global_step": self.global_step,
            "train/trues": trues,
            "train/preds": preds,
            "train/accuracy": acc,
            "train/loss": all_loss,
            "train/f1_score": f1_score,
            "train/cm": cm,
            "train/duration": duration,
        })
        self.global_epoch += 1
        return outputs
    
    def evaluate_with_minibatches(self, minibatch):
        start = timeit.default_timer()
        preds, trues, losses, outputs = ([], [], [], {})
        logger.info("-----begin evaluation/validation-----")
        for x, y, w, sl, re in minibatch:
            # logger.info(fr"eval\valid feature size: {x.shape}")
            x = tf.convert_to_tensor(x)
            x = x[:, :, tf.newaxis]
            y = tf.convert_to_tensor(y)
            w = tf.convert_to_tensor(w)
            y_pred_temp = self.Model(x)
            # print(f"y_pred_temp shape={y_pred_temp.shape}")
            # y_pred = tf.argmax(y_pred_temp, axis=1)
            # print(f"y_pred shape={y_pred.shape}")
            # print(f"y sample {y[:]}")
            loss = self.loss_fn(y_true=y, y_pred=y_pred_temp)
            # loss = loss[:, tf.newaxis]
            # print(f"loss shape {loss.shape}")
            # print(f"weight shape {w.shape}")
            # w = w[tf.newaxis,:]
            loss = tf.multiply(loss, w)
            # y_pred_temp = self.model.call(x)
            # # print(f"y_pred_temp shape={y_pred_temp.shape}")
            # # y_pred = tf.argmax(y_pred_temp, axis=1)
            # # print(f"y_pred shape={y_pred.shape}")
            # # print(f"y sample {y[:]}")
            # loss = self.loss_fn(y_true=y, y_pred=y_pred_temp, from_logits=True)
            # loss = loss[:, tf.newaxis]
            # # print(f"loss shape {loss.shape}")
            # # print(f"weight shape {w.shape}")
            # w = w[tf.newaxis,:]
            # loss = tf.matmul(loss, w)

            sample_weight = tf.reduce_sum(
                tf.multiply(
                    tf.one_hot(indices=y, depth=self.config["n_classes"]),
                    np.asarray(self.config["class_weights"], dtype=np.float32)
                ), 1
            )
            
            loss_w_class = tf.multiply(loss, sample_weight)
            loss = tf.reduce_sum(loss_w_class) / tf.reduce_sum(w)

            losses.append(loss)
            tmp_pred = np.reshape(tf.argmax(y_pred_temp, axis=1), (self.config['batch_size'], self.config['seq_length']))
            temp_trues = np.reshape(y, (self.config["batch_size"], self.config["seq_length"]))

            for i in range(self.config["batch_size"]):
                preds.extend(tmp_pred[i, :sl[i]])
                trues.extend(temp_trues[i, :sl[i]])

        acc = skmetrics.accuracy_score(y_true=trues, y_pred=preds)
        all_loss = np.array(losses).mean()
        f1_score = skmetrics.f1_score(y_true=trues, y_pred=preds, average="macro")
        cm = skmetrics.confusion_matrix(y_true=trues, y_pred=preds, labels=[0, 1, 2, 3, 4])
        stop = timeit.default_timer()
        duration = stop - start
        logger.info(f"time elapsed: {duration}s")
        outputs = {
            "test/trues": trues,
            "test/preds": preds,
            "test/loss": all_loss,
            "test/accuracy": acc,
            "test/f1_score": f1_score,
            "test/cm": cm,
            "test/duration": duration,
        }
        return outputs
        
    def save_best_checkpoint(self, name):
        # cp_callback = keras.callbacks.ModelCheckpoint(filepath=checkpoint_path, verbose=1, save_best_only=True)   
        if not os.path.exists(self.best_ckpt_path):
            os.makedirs(self.best_ckpt_path)
        # save_path = os.path.join(self.best_ckpt_path, "{}.keras".format(name))
        save_path = self.ckpt_mngr.save()
        logger.info("Saved best checkpoint to {}".format(save_path))
          
    
    def get_final_state(self):
        return self.last_state
    
    def get_current_epoch(self):
        return self.global_epoch
    
    def pass_one_epoch(self):
        self.global_epoch = self.global_epoch + 1

# model.add(keras.Input(shape=(len(raw['x']), )))
# model(np.expand_dims(raw['x'][0:1], axis=2))
# model.summary()
# print(raw['x'].shape)


# class convBlock(keras.layers.Layer):
#     def __init__(self, filters, strides, kernel_size):
#         super().__init__()
#         self.conv = keras.layers.Conv1D(filters=int(filters), strides=int(strides), kernel_size=int(kernel_size), use_bias=False, padding='same')
#         self.batch_norm = keras.layers.BatchNormalization()
#         self.relu = keras.layers.LeakyReLU()

#     def call(self, inputs):
#         x = self.conv(inputs)
#         x = self.batch_norm(x)
#         return self.relu(x)

# class TestModel(keras.Model):
#     def __init__(self, is_training=False):
#         super().__init__()
#         # defining representation learning layer
#         self.conv1 = convBlock(filters=128, kernel_size=fs/2, strides=fs/4)
#         self.max_pool1 = keras.layers.MaxPooling1D(pool_size=8, strides=8, padding='same')
#         self.dropout1 = keras.layers.Dropout(0.5)
#         self.conv2 = convBlock(filters=128, strides=1, kernel_size=8)
#         self.conv3 = convBlock(128, 1, 8)
#         self.conv4 = convBlock(128, 1, 8)
#         self.max_pool2 = keras.layers.MaxPooling1D(4, 4, padding='same')
#         self.dropout2 = keras.layers.Dropout(0.5)
#         self.classifier = keras.layers.Dense(5, "softmax",use_bias=False, kernel_initializer=keras.initializers.VarianceScaling(
#             scale=0.1,
#             mode='fan_in',
#             distribution='normal'
#         ))

#     def call(self, inputs):
#         x = self.conv1(inputs)
#         x = self.max_pool1(x)
#         x = self.dropout1(x)
#         x = self.conv2(x)
#         x = self.conv3(x)
#         x = self.conv4(x)
#         x = self.max_pool2(x)
#         x = self.dropout2(x)
#         return self.classifier(x)
    
# model.compile(optimizer='adam',loss=keras.losses.SparseCategoricalCrossentropy(from_logits=True), metrics='accuracy')

# fit_log_dir = r'../log/' + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
# tensorboard_callback = keras.callbacks.TensorBoard(log_dir=fit_log_dir, histogram_freq=1)
# model.fit(train_x[:], train_y, epochs=1, callbacks=[tensorboard_callback])
# print(raw['y'])
# print(train_x.shape)
# model = Network(config=None)
# input = keras.layers.Input(batch_size=9000, shape=(3000, 1))

# model.summary(expand_nested=True)
# loss_fn = keras.losses.SparseCategoricalCrossentropy(from_logits=True)
# loss_fn(y_true=train_y[:1], y_pred=model(train_x[:1])).numpy()

if __name__ == "__main__":
    from sleepedfx import train
    model = Network(config=train)
    input=keras.layers.Input(shape=(3000,1), batch_size=1)
    model.Model(input)
    model.Model.summary()
    print(model.Model)