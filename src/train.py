import argparse
import glob
import importlib
import os
import numpy as np
import shutil
import mne
import tensorflow as tf
import matplotlib.pyplot as plt
import datetime

from utils import load_seq_ids, print_n_samples_each_class
from dataloader import *
from nn import Network
from minibatching import *

import logging
logger = logging.getLogger(__name__)
train_log = r"../log/train.log"
# train_log_dir = r"../log/train/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
os.makedirs(os.path.dirname(train_log), exist_ok=True)
logging.basicConfig(filename=train_log, 
                    filemode="a",
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.INFO
)

def train(config_file, output_dir, fold_idx, restart=False, random_seed=42, use_best=False):
    spec = importlib.util.spec_from_file_location('*', config_file)
    config = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(config)
    config = config.train

    output_dir = os.path.join(output_dir, str(fold_idx))
    if restart:
        if os.path.exists(output_dir):
            shutil.rmtree(output_dir)
        os.makedirs(output_dir)
    else:
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

    subject_files = glob.glob(os.path.join(config["data_dir"], "*.npz"))

    # Load subject ID
    fname = "{}.txt".format(config["dataset"])
    seq_sids = load_seq_ids(fname)

    logger.info("Load generated SIDs from {}".format(fname))
    logger.info("SIDs ({}): {}".format(len(seq_sids), seq_sids))

    # fold_pids = np.array_split(seq_sids, config["n_folds"])

    # test_sids = fold_pids[fold_idx]
    # train_sids = np.setdiff1d(seq_sids, test_sids)

    train_size = int(0.7*len(seq_sids))
    # test_size = len(seq_sids) - train_size
    train_sids = seq_sids[:train_size]
    test_sids = np.setdiff1d(seq_sids, train_sids)
    #splitting 10% of training set as validation set
    n_valids = round(len(train_sids) * 0.10)

    # set random seed
    np.random.seed(random_seed)
    valid_sids = np.random.choice(train_sids, size=n_valids, replace=False)
    train_sids = np.setdiff1d(train_sids, valid_sids)    

    logger.info("Train SIDs: ({}) {}".format(len(train_sids), train_sids))
    logger.info("Valid SIDs: ({}) {}".format(len(valid_sids), valid_sids))
    logger.info("Test SIDs: ({}) {}".format(len(test_sids), test_sids))

    # Get corresponding files
    train_files = []
    for sid in train_sids:
        train_files.append(get_subject_files(
            files=subject_files,
            sid=sid,
        ))
    train_files = np.hstack(train_files)
    train_x, train_y, _ = load_data(train_files)

    valid_files = []
    for sid in valid_sids:
        valid_files.append(get_subject_files(
            files=subject_files,
            sid=sid,
        ))
    valid_files = np.hstack(valid_files)
    valid_x, valid_y, _ = load_data(valid_files)

    test_files = []
    for sid in test_sids:
        test_files.append(get_subject_files(
            files=subject_files,
            sid=sid,
        ))
    test_files = np.hstack(test_files)
    test_x, test_y, _ = load_data(test_files)

    # Print training, validation and test sets
    logger.info("Training set (n_night_sleeps={})".format(len(train_y)))
    print("-----samples of train_y-----")
    for _x in train_x: logger.info(_x.shape)
    print_n_samples_each_class(np.hstack(train_y))

    logger.info("Validation set (n_night_sleeps={})".format(len(valid_y)))
    print("-----samples of valid_y-----")
    for _x in valid_x: logger.info(_x.shape)
    print_n_samples_each_class(np.hstack(valid_y))

    logger.info("Test set (n_night_sleeps={})".format(len(test_y)))
    print("-----samples of test_y-----")
    for _x in test_x: logger.info(_x.shape)
    print_n_samples_each_class(np.hstack(test_y))

    # adding class weight for loss calculation
    # push N1 weight to 1.5
    if config.get('weighted_cross_ent') is None:
        config['weighted_cross_ent'] = False
        logger.info(f'  Weighted cross entropy: Not specified --> default: {config["weighted_cross_ent"]}')
    else:
        logger.info(f'  Weighted cross entropy: {config["weighted_cross_ent"]}')
    if config['weighted_cross_ent']:
        config["class_weights"] = np.asarray([1., 1.5, 1., 1., 1.], dtype=np.float32)
    else:
        config["class_weights"] = np.asarray([1., 1., 1., 1., 1.], dtype=np.float32)
    logger.info(f'  Weighted cross entropy: {config["class_weights"]}')

    model = Network(
        config=config,
        output_dir=output_dir,
        use_best=use_best
    )

    # #inserted model.freeze for debugging
    # model.trainable=False
    # assert model.trainable == False, "couldn't set"

    logger.info('Data Augmentation')
    if config.get('augment_seq') is None:
        config['augment_seq'] = False
        logger.info(f'  Sequence: Not specified --> default: {config["augment_seq"]}')
    else:
        logger.info(f'  Sequence: {config["augment_seq"]}')

    if config.get('augment_signal') is None:
        config['augment_signal'] = False
        logger.info(f'  Signal: Not specified --> default: {config["augment_signal"]}')
    else:
        logger.info(f'  Signal: {config["augment_signal"]}')

    if config.get('augment_signal_full') is None:
        config['augment_signal_full'] = False
        logger.info(f'  Signal full: Not specified --> default: {config["augment_signal_full"]}')
    else:
        logger.info(f'  Signal full: {config["augment_signal_full"]}')

    if config.get('augment_signal') and config.get('augment_signal_full'):
        raise Exception('augment_signal and augment_signal_full cannot be True together.!!')

    best_acc = -1
    best_mf1 = -1
    for epoch in range(model.get_current_epoch(), config["n_epochs"]):
        # Create minibatches for training
        shuffle_idx = np.random.permutation(np.arange(len(train_x)))  # shuffle every epoch is good for generalization
        # Create augmented data
        percent = 0.1
        aug_train_x = np.copy(np.array(train_x, dtype="object"))
        aug_train_y = np.copy(np.array(train_y, dtype="object"))
        for i in range(len(aug_train_x)):
            # Shift signals horizontally
            offset = np.random.uniform(-percent, percent) * aug_train_x[i].shape[1]
            roll_x = np.roll(aug_train_x[i], int(offset))
            if offset < 0:
                aug_train_x[i] = roll_x[:-1]
                aug_train_y[i] = aug_train_y[i][:-1]
            if offset > 0:
                aug_train_x[i] = roll_x[1:]
                aug_train_y[i] = aug_train_y[i][1:]
            roll_x = None
            assert len(aug_train_x[i]) == len(aug_train_y[i])

        # logger.info(f"aug train x shape: {aug_train_x.shape}")
        # logger.info(f"aug train y shape: {aug_train_y.shape}")
        # logger.info(f"test x shape: {test_x.shape}")
        # logger.info(f"test y shape: {test_y.shape}")
        # logger.info(f"valid x shape: {valid_x.shape}")
        # logger.info(f"valid y shape: {valid_y.shape}")

        
        aug_minibatch_fn = iterate_batch_multiple_seq_minibatches(
            aug_train_x,
            aug_train_y,
            batch_size=config["batch_size"],
            seq_length=config["seq_length"],
            shuffle_idx=shuffle_idx,
            augment_seq=config['augment_seq'],
        )
        # Train, one epoch,
        train_outs = model.train_with_minibatches(aug_minibatch_fn)  
        # Create minibatches for validation
        valid_minibatch_fn = iterate_batch_multiple_seq_minibatches(
            np.asarray(valid_x, dtype='object'),
            np.asarray(valid_y, dtype='object'),
            batch_size=config["batch_size"],
            seq_length=config["seq_length"],
            shuffle_idx=None,
            augment_seq=False,
        )
        valid_outs = model.evaluate_with_minibatches(valid_minibatch_fn)
        # Create minibatches for testing
        test_minibatch_fn = iterate_batch_multiple_seq_minibatches(
            np.asarray(test_x, dtype='object'),
            np.asarray(test_y, dtype='object'),
            batch_size=config["batch_size"],
            seq_length=config["seq_length"],
            shuffle_idx=None,
            augment_seq=False,
        )
        test_outs = model.evaluate_with_minibatches(test_minibatch_fn)

        with model.train_writer.as_default():
        # writer = model.train_writer
            tf.summary.scalar(name="e_losses/train", data=train_outs["train/loss"], step=train_outs["global_step"])
            tf.summary.scalar(name="e_losses/valid", data=valid_outs["test/loss"], step=train_outs["global_step"])
            tf.summary.scalar(name="e_losses/test", data=test_outs["test/loss"], step=train_outs["global_step"])
            tf.summary.scalar(name="e_losses/epoch", data=epoch + 1, step=train_outs["global_step"])
            tf.summary.scalar(name="e_accuracy/train", data=train_outs["train/accuracy"], step=train_outs["global_step"])
            tf.summary.scalar(name="e_accuracy/valid", data=valid_outs["test/accuracy"], step=train_outs["global_step"])
            tf.summary.scalar(name="e_accuracy/test", data=test_outs["test/accuracy"], step=train_outs["global_step"])
            tf.summary.scalar(name="e_accuracy/epoch", data=epoch + 1, step=train_outs["global_step"])
            tf.summary.scalar(name="e_f1_score/train", data=train_outs["train/f1_score"], step=train_outs["global_step"])
            tf.summary.scalar(name="e_f1_score/valid", data=valid_outs["test/f1_score"], step=train_outs["global_step"])
            tf.summary.scalar(name="e_f1_score/test", data=test_outs["test/f1_score"], step=train_outs["global_step"])
            tf.summary.scalar(name="e_f1_score/epoch", data=epoch + 1, step=train_outs["global_step"])

        logger.info("[e{}/{} s{}] TR (n={}) l={:.4f} a={:.1f} f1={:.1f} ({:.1f}s)| "
            "VA (n={}) l={:.4f} a={:.1f}, f1={:.1f} ({:.1f}s) | "
            "TE (n={}) l={:.4f} a={:.1f}, f1={:.1f} ({:.1f}s)".format(
            epoch+1,
            config["n_epochs"],
            train_outs["global_step"],
            len(train_outs["train/trues"]),
            train_outs["train/loss"],
            train_outs["train/accuracy"] * 100,
            train_outs["train/f1_score"] * 100,
            train_outs["train/duration"],

            len(valid_outs["test/trues"]),
            valid_outs["test/loss"],
            valid_outs["test/accuracy"] * 100,
            valid_outs["test/f1_score"] * 100,
            valid_outs["test/duration"],

            len(test_outs["test/trues"]),
            test_outs["test/loss"],
            test_outs["test/accuracy"] * 100,
            test_outs["test/f1_score"] * 100,
            test_outs["test/duration"],
            )
        )

        if best_acc < valid_outs["test/accuracy"] and best_mf1 <= valid_outs["test/f1_score"]:
            best_acc = valid_outs["test/accuracy"]
            best_mf1 = valid_outs["test/f1_score"]
            model.save_best_checkpoint(name="best_model")

        if (epoch+1) % config["evaluate_span"] == 0 or (epoch+1) == config["n_epochs"]:
            logger.info(">> Confusion Matrix")
            logger.info(test_outs["test/cm"])

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--config_file", type=str, required=True)
    parser.add_argument("--fold_idx", type=int, required=True)
    parser.add_argument("--output_dir", type=str, default="./output/train")
    parser.add_argument("--restart", dest="restart", action="store_true")
    parser.add_argument("--no-restart", dest="restart", action="store_false")
    parser.add_argument("--log_file", type=str, default="./output/output.log")
    parser.add_argument("--random_seed", type=int, default=42)
    parser.set_defaults(restart=False)
    args = parser.parse_args()

    train(
        config_file=args.config_file,
        fold_idx=args.fold_idx,
        output_dir=args.output_dir,
        log_file=args.log_file,
        restart=args.restart,
        random_seed=args.random_seed,
    )