from improv import PropModel, aggregateData
from sklearn.metrics import classification_report, confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sns
from sleepedfx import train
import numpy as np
import pandas as pd
import tensorflow as tf
from scipy.signal import savgol_filter
from scipy.fft import fft, fftshift
_, _, _, _, test_x, test_y = aggregateData()

# model = EEGNet(nb_classes = 5, Chans = train["n_channels"], Samples = train["input_size"],
#             dropoutRate = 0.5, kernLength = 32, F1 = 8, D = 2, F2 = 16,
#             dropoutType = 'Dropout')
model = PropModel(0.5)
print(model)
model.load_weights(r"C:\Users\Nguyen\Desktop\CRNN-reimplement\src\src\out_sleepedfx\train\prop-2\prop-2.keras")
# test_x = np.expand_dims(test_x, axis=1)
test_x = np.expand_dims(test_x, axis=-1)
# # dataset = tf.data.Dataset.from_tensor_slices(test_x)

# # print(dataset)
# print(np.shape(test_x))
y_pred = model.predict(test_x)
y_pred = np.argmax(y_pred, axis=-1)

print(classification_report(y_true=test_y,y_pred=y_pred,target_names=['WAKE', 'NREM1', 'NREM2', 'NREM3', 'REM']))
cm = confusion_matrix(test_y, y_pred)
cm = pd.DataFrame(cm, range(5),range(5))
plt.figure(figsize = (8,6))

sns.heatmap(cm, annot=True, fmt="d", cmap="Blues", xticklabels=["WAKE", "NREM1", "NREM2", "NREM3", "REM"], yticklabels=["WAKE", "NREM1", "NREM2", "NREM3", "REM"]), # font size
plt.xlabel("Dự đoán")
plt.ylabel("Thực tế")
plt.show()
print(model.layers)