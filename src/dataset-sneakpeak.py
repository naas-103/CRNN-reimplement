
from mne.io import read_raw_edf
import mne
import mne.viz
import matplotlib
matplotlib.use('TkAgg')

raw_file = r"C:\Users\Khoi Nguyen\Documents\KLTN\data\sleepedf\sleep-cassette\SC4122E0-PSG.edf"
anno_file = r"C:\Users\Khoi Nguyen\Documents\KLTN\data\sleepedf\sleep-cassette\SC4122EV-Hypnogram.edf"
raw = read_raw_edf(raw_file, preload=True, stim_channel="Event marker", infer_types=True)
anno = mne.read_annotations(anno_file)

raw.set_annotations(anno, emit_warning=False)

print(raw.info)
'''annotation_desc_2_event_id = {
    "Sleep stage W": 1,
    "Sleep stage 1": 2,
    "Sleep stage 2": 3,
    "Sleep stage 3": 4,
    "Sleep stage 4": 4,
    "Sleep stage R": 5,
}
anno.crop(anno[1]["onset"] - 30 * 60, anno[-2]["onset"] + 30 * 60)
events, _ = mne.events_from_annotations(raw, event_id=annotation_desc_2_event_id, chunk_duration=60.0)

event_id = {
    "Sleep stage W": 1,
    "Sleep stage 1": 2,
    "Sleep stage 2": 3,
    "Sleep stage 3/4": 4,
    "Sleep stage R": 5,
}

fig = mne.viz.plot_events(
    events,
    event_id=event_id,
    sfreq=raw.info["sfreq"],
    first_samp=events[0, 0],
)
stage_colors = plt.rcParams["axes.prop_cycle"].by_key()["color"]

raw.plot(
    start=60,
    duration=60,
    scalings=dict(eeg=1e-4,resp=1e3,eog=1e-4,misc=1e-1)
)

tmax = 60.0 - 1.0 / raw.info["sfreq"]

epochs = mne.Epochs(
    raw=raw, 
    events=events,
    event_id=event_id,
    tmin=0.0,
    tmax=tmax,
    baseline=None,
)

del raw
mne.viz.plot_epochs(epochs, scalings='auto')
'''

#epochs.plot(n_epochs=1, scalings="auto", block=True)
#epochs = mne.Epochs(raw, events, tmin=-1, tmax=4)
#events_from_file = mne.read_events(anno_file)