from baseline import aggregateData
from sleepedfx import train
import numpy as np
import pandas as pd
import tensorflow as tf
import keras
from keras import layers
import numpy as np
import matplotlib.pyplot as plt
_, _, _, _, test_x, test_y = aggregateData()
fs = train['sampling_rate']

def build_model():
    model = keras.Sequential(
            [   
                # layers.Conv1D(filters=128, kernel_size=int(fs/2), strides=int(fs/16), use_bias=False, padding='same'),
                # layers.BatchNormalization(),
                layers.Conv1D(filters=128, kernel_size=int(fs/2), strides=int(fs/16), use_bias=False, padding='same'),
                layers.BatchNormalization(),
                layers.Activation(tf.nn.leaky_relu),

                layers.MaxPooling1D(pool_size=8, strides=8),
                layers.Dropout(rate=0.5),

                layers.Conv1D(filters=128, kernel_size=8, strides=1, use_bias=False, padding='same'),
                layers.BatchNormalization(),
                layers.Activation(tf.nn.leaky_relu),

                layers.Conv1D(filters=128, kernel_size=8, strides=1, use_bias=False, padding='same'),
                layers.BatchNormalization(),
                layers.Activation(tf.nn.leaky_relu),

                layers.Conv1D(filters=128, kernel_size=8, strides=1, use_bias=False, padding='same'),
                layers.BatchNormalization(),
                layers.Activation(tf.nn.leaky_relu),

                layers.MaxPooling1D(pool_size=4, strides=4, padding='same'),
                # layers.Flatten(),
                layers.Dropout(rate=0.5), 

                # layers.RNN(cell=layers.LSTMCell(self.config['n_rnn_units'], unit_forget_bias=1.0), return_sequences=False, return_state=True, stateful=True),
                layers.LSTM(units=train['n_rnn_units']),
                layers.Dropout(0.5),
                layers.Dense(5, use_bias=False, kernel_initializer=keras.initializers.VarianceScaling(scale=0.1, mode='fan_in', distribution='normal'), activation="softmax"),
            ], name="Classificator"
        )
    model.compile(
        optimizer="adam",
        loss="categorical_crossentropy",
        metrics=['accuracy']
    )
    return model

def main():
    model = build_model()
    model(keras.Input((3000,1), 1))
    model.load_weights(r"C:\Users\Nguyen\Desktop\CRNN-reimplement\src\src\out_sleepedfx\train\prop\prop.keras")
    data = np.expand_dims(test_x[0], axis=(0,2))
    out = model.layers[0](data)

    fig, axs = plt.subplots(2,1,figsize=(10, 6))
    axs[0].plot(test_x[0])
    axs[1].plot(out)
    plt.show()
    # print(np.shape(test_x))

if __name__ == "__main__":
    main()