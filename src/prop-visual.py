import numpy as np
import matplotlib.pyplot as plt

data = np.load(r"C:\Users\Nguyen\Desktop\CRNN-reimplement\src\src\out_sleepedfx\train\prop\predict\SC4572F0-pred.npz")
fig, ax = plt.subplots(2)

ax[0].plot(data['y_true'], 'tab:orange')
ax[0].set_title("Gán nhãn bởi chuyên gia")
ax[1].plot(data['y_pred'])
ax[1].set_title("Gán nhãn bởi mô hình")
fig.suptitle("Biểu đồ ngủ")
# plt.legend()
plt.show()