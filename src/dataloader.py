import numpy as np 

import os
import re

def get_subject_files(files, sid):
    reg = f"S[C|T][4|7]{str(sid).zfill(2)}[a-zA-Z0-9]+\.npz$"
    subject_files = []
    for idx, f in enumerate(files):
        ptrn = re.compile(reg)
        if ptrn.search(f):
            subject_files.append(f)

    return subject_files

def load_data(files):
    signals = []
    labels = []
    fs = None

    for file in files:
        with np.load(file) as f:
            x = f["x"]
            y = f["y"]
            fs = f["fs"]

            if fs == None:
                raise Exception(f"Something wrong, no sampling rate found in {file}")
            
            x = x.astype(np.float32)
            y = y.astype(np.int32)

            signals.append(x)
            labels.append(y)

    return signals, labels, fs
