import tensorflow as tf
from keras.constraints import max_norm
from keras.layers import SpatialDropout2D, Dropout, Input, Flatten, BatchNormalization, SeparableConv2D, DepthwiseConv2D, Conv2D, MaxPooling2D, AveragePooling2D, Activation, Dense
import glob
import keras
from utils import load_seq_ids
from dataloader import *
import logging
from sleepedfx import train
from sklearn.metrics import classification_report, confusion_matrix
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from minibatching import *
from keras import layers

logger = logging.getLogger(__name__)
log_file = "../log/baseline.log"
os.makedirs(os.path.dirname(log_file), exist_ok=True)
logging.basicConfig(filename=log_file, 
                    filemode="a",
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.INFO
)
# def print_all_list_len(x):
#     all = 0
#     for arr in x:
#         all += len(arr)
#     return all

def minibatch(x_train, y_train, batch_size, shuffle_idx=None):
    while True:
        for i in range(0, len(x_train), batch_size):
            if shuffle_idx is None:
                x_batch = x_train[i:i+batch_size]
                x_batch = np.expand_dims(x_batch, axis=-1)
                y_batch = y_train[i:i+batch_size]
                y_batch = tf.one_hot(indices=y_batch, depth=train["n_classes"])
            else:
                epoch_idx = shuffle_idx[i:i+batch_size]
                # epoch_idx = epoch_idx.astype(int)
                # print(epoch_idx.dtype)
                x_batch = [x_train[i] for i in epoch_idx]
                y_batch = [y_train[i] for i in epoch_idx]
                x_batch = np.expand_dims(x_batch, axis=-1)         
                y_batch = tf.one_hot(indices=y_batch, depth=train["n_classes"])
            # yield tf.type_spec_from_value(x_batch), tf.type_spec_from_value(y_batch)
            yield x_batch, y_batch

# def test_minibatch(x, batch_size):
#     for i in range(0, len(x), batch_size):
#         x_batch = x[i:i+batch_size]
#         x_batch = np.expand_dims(x_batch, axis=1)
#         x_batch = np.expand_dims(x_batch, axis=1)
#         x_batch = tuple(x_batch)
#         yield x_batch

def reshape(x, squash=False):
    temp = []
    if squash == True:
        return np.concatenate(x, axis=0)
    else:
        for file in x:
            for arr in file:
                temp.append(arr)
        return temp
def PropModel(droprate):
    input = layers.Input(shape=(3000,1))
    block1 = layers.Conv1D(filters=64,
                           kernel_size=50,
                           strides=6,
                           use_bias=False,
                           padding='same'
                           )(input)
    block1 = layers.BatchNormalization()(block1)
    block1 = layers.Activation("relu")(block1)
    block1 = layers.MaxPooling1D(8,2,'same')(block1)
    block1 = layers.Dropout(droprate)(block1)
    block1 = layers.Conv1D(128, kernel_size=8, strides=2, padding='same', use_bias=False)(block1)
    block1 = layers.BatchNormalization()(block1)
    block1 = layers.Activation("relu")(block1)
    block1 = layers.Conv1D(128, kernel_size=8, strides=2, padding='same', use_bias=False)(block1)
    block1 = layers.BatchNormalization()(block1)
    block1 = layers.Activation("relu")(block1)
    block1 = layers.MaxPooling1D(4,4,'same')(block1)
    

    ########################################

    block2 = layers.Conv1D(filters=64,
                        kernel_size=400,
                        strides=49,
                        use_bias=False,
                        padding='same'
                        )(input)
    block2 = layers.BatchNormalization()(block2)
    block2 = layers.Activation('relu')(block2)
    block2 = layers.MaxPooling1D(4,2,'same')(block2)
    block2 = layers.Dropout(droprate)(block2)
    block2 = layers.Conv1D(128,7,1,'same',use_bias=False)(block2)
    block2 = layers.BatchNormalization()(block2)
    block2 = layers.Activation('relu')(block2)
    block2 = layers.Conv1D(128,7,1,'same',use_bias=False)(block2)
    block2 = layers.BatchNormalization()(block2)
    block2 = layers.Activation('relu')(block2)
    block2 = layers.MaxPooling1D(2,2,'same')(block2)

    ########################################
    # block2 = layers.Flatten()(block2)


    block = layers.Concatenate()([block1, block2])
    block = layers.Dropout(droprate)(block)
    block = layers.LSTM(128)(block)
    block = layers.Dense(5, 'softmax')(block)
    return keras.Model(inputs=input, outputs=block)


# def EEGNet(nb_classes, Chans = 64, Samples = 128, 
#              dropoutRate = 0.5, kernLength = 64, F1 = 8, 
#              D = 2, F2 = 16, norm_rate = 0.25, dropoutType = 'Dropout'):
#     if dropoutType == 'SpatialDropout2D':
#         dropoutType = SpatialDropout2D
#     elif dropoutType == 'Dropout':
#         dropoutType = Dropout
#     else:
#         raise ValueError('dropoutType must be one of SpatialDropout2D '
#                          'or Dropout, passed as a string.')
    
#     input1   = Input(shape = (Chans, Samples, 1))

#     ##################################################################
#     block1       = Conv2D(F1, (1, kernLength), padding = 'same',
#                                    use_bias = False)(input1)
#     block1       = BatchNormalization()(block1)
#     block1       = DepthwiseConv2D((Chans, 1), use_bias = False, 
#                                    depth_multiplier = D,
#                                    depthwise_constraint = max_norm(1.))(block1)
#     block1       = BatchNormalization()(block1)
#     block1       = Activation('elu')(block1)
#     block1       = AveragePooling2D((1, 4))(block1)
#     block1       = dropoutType(dropoutRate)(block1)
    
#     block2       = SeparableConv2D(F2, (1, 16),
#                                    use_bias = False, padding = 'same')(block1)
#     block2       = BatchNormalization()(block2)
#     block2       = Activation('elu')(block2)
#     block2       = AveragePooling2D((1, 8))(block2)
#     block2       = dropoutType(dropoutRate)(block2)
        
#     flatten      = Flatten(name = 'flatten')(block2)
    
#     dense        = Dense(nb_classes, name = 'dense', 
#                          kernel_constraint = max_norm(norm_rate))(flatten)
#     softmax      = Activation('softmax', name = 'softmax')(dense)
    
#     return keras.Model(inputs=input1, outputs=softmax)
# 
# model.compile(loss='categorical_crossentropy', optimizer='adam', 
#               metrics = ['accuracy'])
def aggregateData():
    subject_files = glob.glob(os.path.join(train["data_dir"], "*.npz"))

    # Load subject ID
    fname = "{}.txt".format(train["dataset"])
    seq_sids = load_seq_ids(fname)

    logger.info("Load generated SIDs from {}".format(fname))
    logger.info("SIDs ({}): {}".format(len(seq_sids), seq_sids))

    # fold_pids = np.array_split(seq_sids, train["n_folds"])

    # test_sids = fold_pids[fold_idx]
    # train_sids = np.setdiff1d(seq_sids, test_sids)

    train_size = int(0.7*len(seq_sids))
    # test_size = len(seq_sids) - train_size
    train_sids = seq_sids[:train_size]
    test_sids = np.setdiff1d(seq_sids, train_sids)
    #splitting 10% of training set as validation set
    n_valids = round(len(train_sids) * 0.10)

    # set random seed
    np.random.seed(42)
    valid_sids = np.random.choice(train_sids, size=n_valids, replace=False)
    train_sids = np.setdiff1d(train_sids, valid_sids)    

    logger.info("Train SIDs: ({}) {}".format(len(train_sids), train_sids))
    logger.info("Valid SIDs: ({}) {}".format(len(valid_sids), valid_sids))
    logger.info("Test SIDs: ({}) {}".format(len(test_sids), test_sids))

    # Get corresponding files
    train_files = []
    for sid in train_sids:
        train_files.append(get_subject_files(
            files=subject_files,
            sid=sid,
        ))
    train_files = np.hstack(train_files)
    train_x, train_y, _ = load_data(train_files)

    valid_files = []
    for sid in valid_sids:
        valid_files.append(get_subject_files(
            files=subject_files,
            sid=sid,
        ))
    valid_files = np.hstack(valid_files)
    valid_x, valid_y, _ = load_data(valid_files)

    test_files = []
    for sid in test_sids:
        test_files.append(get_subject_files(
            files=subject_files,
            sid=sid,
        ))
    test_files = np.hstack(test_files)
    test_x, test_y, _ = load_data(test_files)
    # print(len(train_x))
    # print(print_all_list_len(train_y))

    reshaped_train_x = reshape(train_x)
    # print(np.shape(reshaped_train_x))
    reshaped_train_y = reshape(train_y, squash=True)
    reshaped_test_x = reshape(test_x)
    reshaped_test_y = reshape(test_y, squash=True)
    reshaped_valid_x = reshape(valid_x)
    reshaped_valid_y = reshape(valid_y, squash=True)

    output = (reshaped_train_x, reshaped_train_y, reshaped_valid_x, reshaped_valid_y, reshaped_test_x, reshaped_test_y)
    
    return output

def main():
    reshaped_train_x, reshaped_train_y, reshaped_valid_x, reshaped_valid_y, _, _ = aggregateData()
    # fs = train['sampling_rate']
    # model = keras.Sequential(
    # [   
    #     # layers.Conv1D(filters=128, kernel_size=int(fs/2), strides=int(fs/16), use_bias=False, padding='same'),
    #     # layers.BatchNormalization(),
    #     layers.Conv1D(filters=128, kernel_size=int(fs/2), strides=int(fs/16), use_bias=False, padding='same'),
    #     layers.BatchNormalization(),
    #     layers.Activation(tf.nn.leaky_relu),

    #     layers.MaxPooling1D(pool_size=8, strides=8),
    #     layers.Dropout(rate=0.5),

    #     layers.Conv1D(filters=128, kernel_size=8, strides=1, use_bias=False, padding='same'),
    #     layers.BatchNormalization(),
    #     layers.Activation(tf.nn.leaky_relu),

    #     layers.Conv1D(filters=128, kernel_size=8, strides=1, use_bias=False, padding='same'),
    #     layers.BatchNormalization(),
    #     layers.Activation(tf.nn.leaky_relu),

    #     layers.Conv1D(filters=128, kernel_size=8, strides=1, use_bias=False, padding='same'),
    #     layers.BatchNormalization(),
    #     layers.Activation(tf.nn.leaky_relu),

    #     layers.MaxPooling1D(pool_size=4, strides=4, padding='same'),
    #     # layers.Flatten(),
    #     layers.Dropout(rate=0.5), 

    #     # layers.RNN(cell=layers.LSTMCell(self.config['n_rnn_units'], unit_forget_bias=1.0), return_sequences=False, return_state=True, stateful=True),
    #     layers.LSTM(units=train['n_rnn_units']),
    #     layers.Dropout(0.5),
    #     layers.Dense(5, use_bias=False, kernel_initializer=keras.initializers.VarianceScaling(scale=0.1, mode='fan_in', distribution='normal'), activation="softmax"),
    # ], name="Classificator"
    # )
    # model(layers.Input((3000,1), 1))
    # model.summary()
    # print(np.shape(reshaped_train_x))
    #call model
    model = PropModel(0.5)
    #compile model
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics = ['accuracy'])

    # #fit model 
    board = keras.callbacks.TensorBoard(r'src\out_sleepedfx\train\prop-2', update_freq=1)
    checkpointer = keras.callbacks.ModelCheckpoint(filepath=r'src\out_sleepedfx\train\prop-2\prop-2.keras', save_best_only=True)
    # fittedModel = model.fit(reshaped_train_x, reshaped_train_y, batch_size = train['batch_size'], epochs = train['n_epochs'], 
    #                         validation_data=(reshaped_valid_x, reshaped_valid_y),
    #                         callbacks=[checkpointer, board], class_weight = class_weights)
    shuffle_idx = np.random.permutation(np.arange(len(reshaped_train_x)))
    model.fit(
        minibatch(reshaped_train_x, reshaped_train_y, batch_size=train["batch_size"], shuffle_idx=shuffle_idx), 
        steps_per_epoch=int(len(reshaped_train_x)/train["batch_size"]),
        epochs=30, 
        batch_size=train['batch_size'],
        callbacks=[checkpointer, board], 
        validation_data=minibatch(reshaped_valid_x, reshaped_valid_y, batch_size=train["batch_size"]),
        validation_steps=int(len(reshaped_valid_x)/train["batch_size"])
    )


if __name__ == "__main__":
    main()